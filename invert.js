// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.

function invert(obj) {
  let object = {};
  for (let key in obj) {
    object[obj[key]] = key;
  }
  return object;
}

module.exports = invert;
