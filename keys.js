// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.

function keys(obj) {
  let array = [];
  for (let key in obj) {
    array.push(key);
  }
  return array;
}
module.exports = keys;
