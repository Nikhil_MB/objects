// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.

function defaults(obj, defaultProps) {
  for (let key in defaultProps) {
    if (!(key in obj)) {
      obj[key] = defaultProps[key];
    }
  }
  return obj;
}

module.exports = defaults;
