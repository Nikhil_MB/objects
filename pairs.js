// Convert an object into a list of [key, value] pairs.

function pairs(obj) {
  let array = [];
  for (let key in obj) {
    array.push([key, obj[key]]);
  }
  return array;
}
module.exports = pairs;
