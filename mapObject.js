// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.

function mapObject(obj, cb) {
  let object = {};
  for (let key in obj) {
    object[key] = cb(obj[key]);
  }
  return object;
}

module.exports = mapObject;
