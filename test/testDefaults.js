const defaults = require("../defaults");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
result = defaults(testObject, { name: "Batman", vehicle: "Batmobile" });

console.log(result);
