const mapObject = require("../mapObject");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const result = mapObject(testObject, (val) => val + 5);

console.log(result);
