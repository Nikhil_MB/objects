// Return all of the values of the object's own properties.
// Ignore functions

function values(obj) {
  let array = [];
  for (let key in obj) {
    array.push(obj[key]);
  }
  return array;
}
module.exports = values;
